﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using IOP.Pulsar.MQTT;

namespace UnitTest
{
    [TestClass]
    public class WildcardTest
    {
        private const string text1 = "/test1/test2/test3";
        private const string text2 = "test1/test2/test3";
        private const string text3 = "text1";
        private const string text4 = "/text1";
        private const string text5 = "text1/";
        private const string text6 = "text1/text2";
        private const string text7 = "text1/text2/";
        private const string text8 = "/text1/text2";
        private const string text9 = "/text1/text2/text3/text4/text5";

        [TestMethod]
        public void WildCardTest()
        {
            try
            {
                var pattern = "+";
                Assert.IsTrue(text3.MatchWildcard(pattern));
                Assert.IsFalse(text4.MatchWildcard(pattern));
                var pattern2 = "/+";
                Assert.IsTrue(text4.MatchWildcard(pattern2));
                Assert.IsFalse(text3.MatchWildcard(pattern2));
                var pattern3 = "+/";
                Assert.IsTrue(text5.MatchWildcard(pattern3));
                Assert.IsFalse(text6.MatchWildcard(pattern3));
                var pattern4 = "+/+";
                Assert.IsTrue(text6.MatchWildcard(pattern4));
                Assert.IsFalse(text8.MatchWildcard(pattern4));
                var pattern5 = "/test1/+/test3";
                Assert.IsTrue(text1.MatchWildcard(pattern5));
                Assert.IsFalse(text2.MatchWildcard(pattern5));
                var pattern6 = "/test1/#/";
                Assert.IsTrue(text1.MatchWildcard(pattern6));
                Assert.IsFalse(text2.MatchWildcard(pattern6));
                var pattern7 = "/+/#";
                Assert.IsTrue(text8.MatchWildcard(pattern7));
                Assert.IsFalse(text2.MatchWildcard(pattern7));
                Assert.IsTrue(text9.MatchWildcard(pattern7));
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
