﻿using IOP.Pulsar.Extensions;
using IOP.Pulsar.Hosting;
using IOP.Pulsar.MQTT;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;

namespace PulsarIntegrationTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await CreateHostBuilder(args).Build().RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
            => Host.CreateDefaultBuilder(args)
            .ConfigurePulsarHost(builder =>
            {
                builder.AddTCPServer<MQTTContext>("testServer");
                builder.UseStartup<Startup>();
            }).UseEnvironment("Development");
    }
}
