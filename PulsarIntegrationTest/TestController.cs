﻿using IOP.Models.Message.MQTT;
using IOP.Models.Message.MQTT.Package;
using IOP.Pulsar.MQTT;
using IOP.Pulsar.MQTT.Attributes;
using IOP.Pulsar.MQTT.Result;
using Microsoft.Extensions.Logging;
using System.Text;

namespace PulsarIntegrationTest
{
    public class TestController : MQTTController
    {
        private readonly ILogger<TestController> _Logger;

        public TestController(ILogger<TestController> logger)
        {
            _Logger = logger;
        }

        [CONNECT]
        public ConnectResult ConnectTest(ConnectPackage package)
        {
            _Logger.LogInformation("Connect");
            return Connack(ReturnCodeType.Accept);
        }

        [PUBLISH("/uav")]
        public PublishResult UAVPublish(PublishPackage package)
        {
            _Logger.LogInformation($"{Context.Session.ClientId} publish : {Encoding.UTF8.GetString(package.Body)}");
            return PublishFinish();
        }

        [PUBREL]
        public PubrelResult PubrelTest(PubrelPackage package)
        {
            _Logger.LogInformation("/pubcomp");
            return Pubrel();
        }

        [SUBSCRIBE]
        public SubscribeResult SubscribeTest(SubscribePackage package)
        {
            _Logger.LogInformation("/subscribe");
            return Suback();
        }

        [UNSUBSCRIBE]
        public UnsubscribeResult UnsubscribeTest(UnsubscribePackage package)
        {
            _Logger.LogInformation("/unsubscribe");
            return Unsuback();
        }

        [PINGREQ]
        public PingreqResult PingreqTest(PingreqPackage pingreq)
        {
            _Logger.LogInformation("pingreq");
            return Pingresp();
        }
    }
}
