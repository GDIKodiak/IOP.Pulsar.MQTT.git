docker stop redismaster ;
docker rm redismaster ;
docker run -p 6379:6379 -p 2333:2333 -d --name redismaster -v f:/docker/redis/data:/data redis:latest --appendonly yes ;