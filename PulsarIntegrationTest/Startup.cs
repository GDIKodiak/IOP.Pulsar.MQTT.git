﻿using IOP.Pulsar.Server;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using IOP.Pulsar.Extensions;
using IOP.Pulsar.MQTT;
using IOP.Extension.Cache;
using IOP.Pulsar.Abstractions;

namespace PulsarIntegrationTest
{
    public class Startup : IStartup
    {

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCacheService();
            //services.AddRedisCacheService(options =>
            //{
            //    options.ConnectionStrings = new string[] { "127.0.0.1:6379,connectTimeout=1000,connectRetry=1,syncTimeout=10000" };
            //});
            services.AddMQTT();
        }


        public void Configure(IServerBuilder server, IHostEnvironment env)
        {
            server.BuildPipeline<MQTTContext>(builder =>
            {
                builder.UseMQTTControlCenter();
            });
        }
    }
}
