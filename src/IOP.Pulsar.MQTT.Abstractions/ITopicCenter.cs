﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 主题中心接口
    /// </summary>
    public interface ITopicCenter
    {
        /// <summary>
        /// 报文缓存
        /// </summary>
        ConcurrentDictionary<ushort, CachePublishPackage> CachePackages { get; }
        /// <summary>
        /// 获取或者创建主题
        /// </summary>
        /// <param name="topicName"></param>
        /// <returns></returns>
        Topic GetOrCreateTopic(string topicName);
        /// <summary>
        /// 获取主题集合
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IEnumerable<Topic> GetTopics(string filter);
        /// <summary>
        /// 创建报文标识
        /// </summary>
        /// <param name="oldValue"></param>
        /// <returns></returns>
        ushort CreatePacketIdentifier(ushort oldValue = 0);
    }
}
