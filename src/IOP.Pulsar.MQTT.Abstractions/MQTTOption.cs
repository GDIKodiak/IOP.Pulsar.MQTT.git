﻿using IOP.Protocols.MQTT;
using IOP.Pulsar.Abstractions;

namespace IOP.Pulsar.MQTT.Abstractions
{
    public class MQTTOption : IClientOptions
    {
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 协议级别
        /// </summary>
        public byte ProtocolLevel { get; set; } = 0x04;
        /// <summary>
        /// 清除会话
        /// </summary>
        public bool CleanSession { get; set; } = false;
        /// <summary>
        /// 保持连接
        /// </summary>
        public ushort KeepAlive { get; set; } = 60;
        /// <summary>
        /// 客户端标识符
        /// </summary>
        public string ClientIdentifier { get; set; } = "";
        /// <summary>
        /// 遗嘱标志
        /// </summary>
        public bool WillFlag { get; set; } = false;
        /// <summary>
        /// 遗嘱QoS
        /// </summary>
        public QoSType WillQoS { get; set; } = QoSType.QoS0;
        /// <summary>
        /// 遗嘱保留
        /// </summary>
        public bool WillRetain { get; set; } = false;
        /// <summary>
        /// 遗嘱主题
        /// </summary>
        public string WillTopic { get; set; } = "";
        /// <summary>
        /// 遗嘱消息
        /// </summary>
        public byte[] WillMessage { get; set; } = new byte[0];
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; } = "";
        /// <summary>
        /// 密码
        /// </summary>
        public byte[] Password { get; set; } = new byte[0];
        /// <summary>
        /// 包重发次数
        /// </summary>
        public int RepeatCount { get; set; } = 3;
        /// <summary>
        /// 包重发间隔
        /// </summary>
        public double RepeatInterval { get; set; } = 10000;
    }
}
