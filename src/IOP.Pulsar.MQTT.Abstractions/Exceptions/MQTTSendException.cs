﻿using IOP.Protocols.MQTT.Package;
using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// MQTT发送错误
    /// </summary>
    public class MQTTSendException : Exception
    {
        /// <summary>
        /// 错误包
        /// </summary>
        public readonly IMQTTPackage Package;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="package"></param>
        /// <param name="message"></param>
        public MQTTSendException(IMQTTPackage package, string message)
            :base(message)
        {
            Package = package;
        }
    }
}
