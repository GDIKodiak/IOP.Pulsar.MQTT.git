﻿using IOP.Protocols.MQTT;
using IOP.Protocols.MQTT.Package;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 缓存报文
    /// </summary>
    public class CachePackage
    {
        /// <summary>
        /// QOS级别
        /// </summary>
        public QoSType QoSType { get; set; }
        /// <summary>
        /// 发布包
        /// </summary>
        public PublishPackage PublishPackage { get; set; }
    }
}
