﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 发布回执标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PUBACKAttribute : Attribute
    {
        /// <summary>
        /// 发布回执标签
        /// </summary>
        public PUBACKAttribute() { }
    }
}
