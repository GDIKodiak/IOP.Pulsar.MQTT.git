﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 发布释放标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PUBCOMPAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public PUBCOMPAttribute() { }
    }
}
