﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 订阅标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SUBSCRIBEAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SUBSCRIBEAttribute()
        {
        }
    }
}
