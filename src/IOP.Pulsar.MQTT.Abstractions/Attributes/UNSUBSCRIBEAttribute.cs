﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 取消订阅标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class UNSUBSCRIBEAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public UNSUBSCRIBEAttribute()
        {
        }
    }
}
