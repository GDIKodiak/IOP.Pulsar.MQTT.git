﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 断开标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class DISCONNECTAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public DISCONNECTAttribute() { }
    }
}
