﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 发布标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PUBLISHAttribute : Attribute
    {
        /// <summary>
        /// 主题过滤器
        /// </summary>
        public string TopicFilter { get; set; } = "";
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="filter">主题过滤器</param>
        public PUBLISHAttribute(string filter)
        {
            TopicFilter = filter;
        }
    }
}
