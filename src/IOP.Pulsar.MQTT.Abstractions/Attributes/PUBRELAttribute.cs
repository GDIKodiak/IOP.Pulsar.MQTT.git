﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 发布释放标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PUBRELAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public PUBRELAttribute() { }
    }
}
