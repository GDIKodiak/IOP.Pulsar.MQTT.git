﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 心跳请求标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PINGREQAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public PINGREQAttribute() { }
    }
}
