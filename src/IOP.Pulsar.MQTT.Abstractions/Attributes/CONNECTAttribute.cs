﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 连接标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class CONNECTAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public CONNECTAttribute() { }
    }
}
