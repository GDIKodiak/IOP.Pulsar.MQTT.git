﻿using System;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 发布收到标签
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PUBRECAttribute : Attribute
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public PUBRECAttribute() { }
    }
}
