﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Pulsar.MQTT.Client
{
    /// <summary>
    /// MQTT客户端异常
    /// </summary>
    public class MQTTClientException : Exception
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        public string Error;
        /// <summary>
        /// 报错的客户端
        /// </summary>
        public MQTTClient Client;
        /// <summary>
        /// 内部错误
        /// </summary>
        public new readonly Exception InnerException;
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="client">客户端</param>
        /// <param name="message">信息</param>
        public MQTTClientException(MQTTClient client, string message) : base(message)
        {
            Error = message;
            Client = client;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="client">客户端</param>
        /// <param name="message">信息</param>
        /// <param name="innerException">内部错误</param>
        public MQTTClientException(MQTTClient client, string message, Exception innerException)
            : base(message, innerException)
        {
            Error = message;
            Client = client;
            InnerException = innerException;
        }
    }
}
