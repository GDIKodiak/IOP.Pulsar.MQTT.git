﻿using IOP.Models.Message.MQTT;
using System;

namespace IOP.Pulsar.MQTT.Client
{
    /// <summary>
    /// MQTT客户端服务接口
    /// </summary>
    public interface IMQTTClientService
    {
        /// <summary>
        /// 连接
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        MQTTClient Connect(Action<MQTTOption> option);
        /// <summary>
        /// 重连
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        MQTTClient Reconnect(MQTTClient client);
        /// <summary>
        /// 重连
        /// </summary>
        /// <param name="client"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        MQTTClient Reconnect(MQTTClient client, Action<MQTTOption> option);
        /// <summary>
        /// 重连
        /// </summary>
        /// <param name="clientName"></param>
        /// <returns></returns>
        MQTTClient Reconnect(string clientName);
        /// <summary>
        /// 重连
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        MQTTClient Reconnect(string clientName, Action<MQTTOption> option);
        /// <summary>
        /// 发送
        /// </summary>
        /// <param name="body">报文</param>
        /// <param name="topicName">主题名</param>
        /// <param name="client">客户端</param>
        /// <param name="qoSType">QoS级别</param>
        /// <param name="retain">重发</param>
        void SendPublish(MQTTClient client, string topicName, byte[] body, QoSType qoSType = QoSType.QoS0, bool retain = false);
        /// <summary>
        /// 发布
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="topicName"></param>
        /// <param name="body"></param>
        /// <param name="qoSType"></param>
        /// <param name="retain"></param>
        void SendPublish(string clientName, string topicName, byte[] body, QoSType qoSType = QoSType.QoS0, bool retain = false);
        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="client"></param>
        /// <param name="topicFilters">主题过滤器</param>
        void SendSubscribe(MQTTClient client, TopicFilter[] topicFilters);
        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="topicFilters"></param>
        void SendSubscribe(string clientName, TopicFilter[] topicFilters);
        /// <summary>
        /// 取消订阅
        /// </summary>
        /// <param name="client"></param>
        /// <param name="topicFilters"></param>
        void SendUnsubscribe(MQTTClient client, string[] topicFilters);
        /// <summary>
        /// 取消订阅
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="topicFilters"></param>
        void SendUnsubscribe(string clientName, string[] topicFilters);
        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="name"></param>
        void Disconnect(string name);
        /// <summary>
        /// 断开连接
        /// </summary>
        /// <param name="client"></param>
        void Disconnect(MQTTClient client);
        /// <summary>
        /// 获得客户端
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        MQTTClient GetClient(string clientId);
        /// <summary>
        /// 删除客户端
        /// </summary>
        /// <param name="client"></param>
        void DeleteClient(MQTTClient client);
        /// <summary>
        /// 删除客户端
        /// </summary>
        /// <param name="clientId"></param>
        void DeleteClient(string clientId);
    }
}
