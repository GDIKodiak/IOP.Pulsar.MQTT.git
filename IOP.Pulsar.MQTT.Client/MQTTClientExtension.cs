﻿using IOP.Extension;
using IOP.Models.Message.MQTT.Package;
using System;

namespace IOP.Pulsar.MQTT.Client
{
    /// <summary>
    /// MQTT客户端扩展
    /// </summary>
    public static class MQTTClientExtension
    {
        /// <summary>
        /// 监听处理函数
        /// </summary>
        /// <param name="client">客户端</param>
        /// <param name="productLine">生产线委托</param>
        /// <returns></returns>
        public static MQTTClient Prod(this MQTTClient client, Action<MQTTClientProductLine> productLine)
        {
            var line = new MQTTClientProductLine();
            productLine(line);
            client.SetPropertyValue("ProductLine", line);
            return client;
        }

        /// <summary>
        /// 连接回执处理函数
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="connack">回执报文委托</param>
        public static void OnConnack(this MQTTClientProductLine productLine, Action<MQTTClient, ConnackPackage> connack)
            => productLine.ConnackHandle = connack;
        /// <summary>
        /// 发布处理函数
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="publish">发布报文委托</param>
        public static void OnPublish(this MQTTClientProductLine productLine, Action<MQTTClient, PublishPackage> publish)
            => productLine.PublishHandle = publish;
        /// <summary>
        /// 发布回执
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="puback">发布回执委托</param>
        public static void OnPuback(this MQTTClientProductLine productLine, Action<MQTTClient, PubackPackage> puback)
            => productLine.PubackHandle = puback;
        /// <summary>
        /// 发布收到
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="pubrec">发布收到委托</param>
        public static void OnPubrec(this MQTTClientProductLine productLine, Action<MQTTClient, PubrecPackage> pubrec)
            => productLine.PubrecHandle = pubrec;
        /// <summary>
        /// 发布释放
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="pubrel">发布释放委托</param>
        public static void OnPubrel(this MQTTClientProductLine productLine, Action<MQTTClient, PubrelPackage> pubrel)
            => productLine.PubrelHandle = pubrel;
        /// <summary>
        /// 发布完成
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="pubcomp">发布完成委托</param>
        public static void OnPubcomp(this MQTTClientProductLine productLine, Action<MQTTClient, PubcompPackage> pubcomp)
            => productLine.PubcompHandle = pubcomp;
        /// <summary>
        /// 订阅回执
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="suback">订阅回执委托</param>
        public static void OnSuback(this MQTTClientProductLine productLine, Action<MQTTClient, SubackPackage> suback)
            => productLine.SubackHandle = suback;
        /// <summary>
        /// 取消订阅回执
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="unsuback">取消订阅回执委托</param>
        public static void OnUnsuback(this MQTTClientProductLine productLine, Action<MQTTClient, UnsubackPackage> unsuback)
            => productLine.UnsubackHandle = unsuback;
        /// <summary>
        /// 心跳回执
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="pingresp">心跳回执委托</param>
        public static void OnPingresp(this MQTTClientProductLine productLine, Action<MQTTClient, PingrespPackage> pingresp)
            => productLine.PingrespHandle = pingresp;
        /// <summary>
        /// 断开回执
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="disconnect">断开回执委托</param>
        public static void OnDisconnect(this MQTTClientProductLine productLine, Action<MQTTClient, DisconnectPackage> disconnect)
            => productLine.DisconnectHandle = disconnect;
        /// <summary>
        /// 错误
        /// </summary>
        /// <param name="productLine">当前生产线</param>
        /// <param name="error">错误委托</param>
        public static void OnError(this MQTTClientProductLine productLine, Action<MQTTClient, Exception> error)
            => productLine.ErrorHandle = error;
    }
}
