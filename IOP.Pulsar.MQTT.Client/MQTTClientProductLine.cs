﻿using IOP.Models.Message.MQTT.Package;
using System;

namespace IOP.Pulsar.MQTT.Client
{
    /// <summary>
    /// MQTT客户端生产线
    /// </summary>
    public class MQTTClientProductLine
    {
        /// <summary>
        /// 连接回执委托
        /// </summary>
        internal Action<MQTTClient, ConnackPackage> ConnackHandle;
        /// <summary>
        /// 发布委托
        /// </summary>
        internal Action<MQTTClient, PublishPackage> PublishHandle;
        /// <summary>
        /// 发布回执委托(QoS1)
        /// </summary>
        internal Action<MQTTClient, PubackPackage> PubackHandle;
        /// <summary>
        /// 发布收到委托
        /// </summary>
        internal Action<MQTTClient, PubrecPackage> PubrecHandle;
        /// <summary>
        /// 发布释放委托
        /// </summary>
        internal Action<MQTTClient, PubrelPackage> PubrelHandle;
        /// <summary>
        /// 发布完成(QoS2)
        /// </summary>
        internal Action<MQTTClient, PubcompPackage> PubcompHandle;
        /// <summary>
        /// 订阅回执
        /// </summary>
        internal Action<MQTTClient, SubackPackage> SubackHandle;
        /// <summary>
        /// 取消订阅回执
        /// </summary>
        internal Action<MQTTClient, UnsubackPackage> UnsubackHandle;
        /// <summary>
        /// 心跳回执
        /// </summary>
        internal Action<MQTTClient, PingrespPackage> PingrespHandle;
        /// <summary>
        /// 断开连接
        /// </summary>
        internal Action<MQTTClient, DisconnectPackage> DisconnectHandle;
        /// <summary>
        /// 错误
        /// </summary>
        internal Action<MQTTClient, Exception> ErrorHandle;
    }
}
