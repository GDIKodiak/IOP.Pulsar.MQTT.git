﻿using System;
using IOP.Extension;

namespace IOP.Pulsar.MQTT.Client
{
    /// <summary>
    /// MQTT客户端服务扩展
    /// </summary>
    public static class MQTTClientServiceExtension
    {
        /// <summary>
        /// 监听处理函数
        /// </summary>
        /// <param name="clientService">客户端服务</param>
        /// <param name="clientName">客户端名</param>
        /// <param name="productLine">生产线委托</param>
        public static void Prod(this MQTTClientService clientService, string clientName, Action<MQTTClientProductLine> productLine)
        {
            var line = new MQTTClientProductLine();
            productLine(line);
            var mqtt = clientService.GetClient(clientName);
            if (mqtt == null) throw new NullReferenceException($"Not find MQTTClient name {clientName}");
            mqtt.SetPropertyValue("ProductLine", line);
        }

        /// <summary>
        /// 监听处理函数
        /// </summary>
        /// <param name="clientService">客户端服务</param>
        /// <param name="client">目标客户端</param>
        /// <param name="productLine">生产线委托</param>
        public static void Prod(this MQTTClientService clientService, MQTTClient client, Action<MQTTClientProductLine> productLine)
        {
            var line = new MQTTClientProductLine();
            productLine(line);
            if (client == null) throw new NullReferenceException($"this MQTTClient is null");
            client.SetPropertyValue("ProductLine", line);
        }
    }
}
