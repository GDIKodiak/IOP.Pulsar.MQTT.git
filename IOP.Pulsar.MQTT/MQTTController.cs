﻿using IOP.Models.Message.MQTT;
using IOP.Models.Message.MQTT.Package;
using IOP.Pulsar.MQTT.Abstractions;
using IOP.Pulsar.MQTT.Result;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// MQTT控制器
    /// </summary>
    public class MQTTController
    {
        /// <summary>
        /// 数据上下文
        /// </summary>
        public MQTTContext Context { get; private set; }

        /// <summary>
        /// 主题中心
        /// </summary>
        public ITopicCenter TopicCenter { get; private set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public MQTTController()
        {
        }

        /// <summary>
        /// 返回连接回执
        /// </summary>
        /// <param name="returnCode"></param>
        /// <returns></returns>
        public virtual ConnectResult Connack(ReturnCodeType returnCode)
        {
            return new ConnectResult(returnCode);
        }
        /// <summary>
        /// 发布结束
        /// </summary>
        /// <returns></returns>
        public virtual PublishResult PublishFinish()
        {
            var pub = (PublishPackage)Context.Request.Package;
            var topic = TopicCenter.GetOrCreateTopic(pub.TopicName);
            topic.Publish(pub);
            return new PublishResult();
        }

        /// <summary>
        /// 集群发布
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public virtual PublishResult ClusterPublish(PublishPackage package)
        {
            return PublishFinish();
        }

        /// <summary>
        /// 发布回执
        /// </summary>
        /// <returns></returns>
        public virtual PubackResult Puback()
        {
            return new PubackResult();
        }

        /// <summary>
        /// QoS2发布释放
        /// </summary>
        /// <returns></returns>
        public virtual PubrecResult Pubrec()
        {
            return new PubrecResult();
        }

        /// <summary>
        /// QoS2发布收到
        /// </summary>
        /// <returns></returns>
        public virtual PubrelResult Pubrel()
        {
            return new PubrelResult();
        }

        /// <summary>
        /// QoS2发布完成
        /// </summary>
        /// <returns></returns>
        public virtual PubcompResult Pubcomp()
        {
            return new PubcompResult();
        }

        /// <summary>
        /// 订阅回执
        /// </summary>
        /// <returns></returns>
        public virtual SubscribeResult Suback()
        {
            var sub = (SubscribePackage)Context.Request.Package;
            var result = Context.Session.Subscribe(sub);
            return new SubscribeResult(result);
        }
        /// <summary>
        /// 取消订阅回执
        /// </summary>
        /// <returns></returns>
        public virtual UnsubscribeResult Unsuback()
        {
            var unSub = (UnsubscribePackage)Context.Request.Package;
            Context.Session.Unsubscribe(unSub);
            return new UnsubscribeResult();
        }

        /// <summary>
        /// 心跳回执
        /// </summary>
        /// <returns></returns>
        public virtual PingreqResult Pingresp()
        {
            return new PingreqResult();
        }

        /// <summary>
        /// 取消连接
        /// </summary>
        /// <returns></returns>
        public virtual DisconnectResult Disconnect()
        {
            return new DisconnectResult();
        }
    }
}
