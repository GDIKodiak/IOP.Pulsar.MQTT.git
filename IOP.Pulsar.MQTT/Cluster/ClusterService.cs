﻿using IOP.Models.Message.MQTT.Package;
using IOP.Pulsar.MQTT.Abstractions;
using IOP.Pulsar.MQTT.Client;
using System;
using System.Collections.Concurrent;
using System.Text;

namespace IOP.Pulsar.MQTT.Cluster
{
    /// <summary>
    /// 集群服务
    /// </summary>
    public class ClusterService : IClusterService
    {
        /// <summary>
        /// 集群客户端
        /// </summary>
        private readonly ConcurrentDictionary<string, MQTTClient> _ClusterClients = new ConcurrentDictionary<string, MQTTClient>();

        

        /// <summary>
        /// 构造函数
        /// </summary>
        public ClusterService() { }

        /// <summary>
        /// 集群发布
        /// </summary>
        /// <param name="package"></param>
        public void ClusterPublish(PublishPackage package)
        {
            throw new NotImplementedException();
        }
    }
}
