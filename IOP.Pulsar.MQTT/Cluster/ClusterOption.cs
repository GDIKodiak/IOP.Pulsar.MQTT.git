﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Pulsar.MQTT.Cluster
{
    /// <summary>
    /// 集群配置
    /// </summary>
    public class ClusterOption : IOptions<ClusterOption>
    {
        /// <summary>
        /// 地址
        /// </summary>
        public string[] Address { get; set; } = new string[0];

        /// <summary>
        /// 保持连接
        /// </summary>
        public ushort KeepAlive { get; set; } = 60;

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; } = "";
        /// <summary>
        /// 密码
        /// </summary>
        public byte[] Password { get; set; } = new byte[0];
        /// <summary>
        /// 包重发次数
        /// </summary>
        public int RepeatCount { get; set; } = 3;

        /// <summary>
        /// 重新连接次数
        /// </summary>
        public int ReconnectCount { get; set; } = 5;

        /// <summary>
        /// 重新连接间隔
        /// </summary>
        public int ReconnectionInterval { get; set; } = 5000;
        /// <summary>
        /// 值
        /// </summary>
        public ClusterOption Value => this;
    }
}
