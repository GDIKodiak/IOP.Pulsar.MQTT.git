﻿using System.Threading.Tasks;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// MQTT结果
    /// </summary>
    public abstract class MQTTResult
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        protected MQTTResult() { }
        /// <summary>
        /// 执行结果
        /// </summary>
        /// <param name="context"></param>
        public virtual void ExcuteResult(MQTTContext context) { }
        /// <summary>
        /// 执行结果(异步)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual Task ExcuteResultAsync(MQTTContext context) => Task.CompletedTask;
    }
}
