﻿using IOP.Models.Message.MQTT.Package;
using System.Threading.Tasks;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// 心跳请求结果
    /// </summary>
    public class PingreqResult : MQTTResult
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="context"></param>
        public override void ExcuteResult(MQTTContext context)
        {
            PingrespPackage pingresp = new PingrespPackage(0);
            context.Response.Package = pingresp;
            context.Response.SendPackage(pingresp);
        }
        /// <summary>
        /// 异步执行
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ExcuteResultAsync(MQTTContext context)
        {
            return Task.Run(() =>
            {
                ExcuteResult(context);
            });
        }
    }
}
