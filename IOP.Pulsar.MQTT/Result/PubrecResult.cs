﻿using IOP.Models.Message.MQTT.Package;
using System.Threading.Tasks;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// 发布收到结果
    /// </summary>
    public class PubrecResult : MQTTResult
    {
        /// <summary>
        /// 执行结果
        /// </summary>
        /// <param name="context"></param>
        public override void ExcuteResult(MQTTContext context)
        {
            var pubrec = (PubrecPackage)context.Request.Package;
            PubrelPackage pubrel = new PubrelPackage(pubrec.PacketIdentifier);
            context.Response.Package = pubrel;
            context.Response.SendPackage(pubrel);
        }
        /// <summary>
        /// 执行结果(异步)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ExcuteResultAsync(MQTTContext context)
        {
            return Task.Run(() =>
            {
                ExcuteResult(context);
            });
        }
    }
}
