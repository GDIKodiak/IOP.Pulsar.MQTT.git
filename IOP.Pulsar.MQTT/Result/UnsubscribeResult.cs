﻿using System.Threading.Tasks;
using IOP.Models.Message.MQTT.Package;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// 取消订阅结果
    /// </summary>
    public class UnsubscribeResult : MQTTResult
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public UnsubscribeResult() { }
        /// <summary>
        /// 执行结果
        /// </summary>
        /// <param name="context"></param>
        public override void ExcuteResult(MQTTContext context)
        {
            var unsubscribe = (UnsubscribePackage)context.Request.Package;
            UnsubackPackage package = new UnsubackPackage(unsubscribe.PacketIdentifier);
            context.Response.Package = package;
            context.Response.SendPackage(package);
        }
        /// <summary>
        /// 执行结果(异步)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ExcuteResultAsync(MQTTContext context)
        {
            return Task.Run(() =>
            {
                ExcuteResult(context);
            });
        }
    }
}
