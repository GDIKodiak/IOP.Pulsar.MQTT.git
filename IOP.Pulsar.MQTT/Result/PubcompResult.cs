﻿using IOP.Models.Message.MQTT.Package;
using System.Threading.Tasks;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// 发布完成结果
    /// </summary>
    public class PubcompResult : MQTTResult
    {
        /// <summary>
        /// 执行
        /// </summary>
        /// <param name="context"></param>
        public override void ExcuteResult(MQTTContext context)
        {
            var package = (PubackPackage)context.Request.Package;
            var cache = context.Session._TopicCenter.CachePackages[package.PacketIdentifier];
            cache?.RepeatTask.Interrupt();
        }
        /// <summary>
        /// 异步执行
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ExcuteResultAsync(MQTTContext context)
        {
            return Task.Run(() =>
            {
                ExcuteResult(context);
            });
        }
    }
}
