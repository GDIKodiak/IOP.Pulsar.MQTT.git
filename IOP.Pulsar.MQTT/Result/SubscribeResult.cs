﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IOP.Models.Message.MQTT;
using IOP.Models.Message.MQTT.Package;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// 订阅结果
    /// </summary>
    public class SubscribeResult : MQTTResult
    {
        /// <summary>
        /// 订阅返回码
        /// </summary>
        public List<SubackCode> Codes { get; set; }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="codes"></param>
        public SubscribeResult(List<SubackCode> codes)
        {
            Codes = codes;
        }
        /// <summary>
        /// 执行结果
        /// </summary>
        /// <param name="context"></param>
        public override void ExcuteResult(MQTTContext context)
        {
            var subscribe = (SubscribePackage)context.Request.Package;
            SubackPackage package = new SubackPackage(subscribe.PacketIdentifier, Codes.ToArray());
            context.Response.Package = package;
            context.Response.SendPackage(package);
        }
        /// <summary>
        /// 执行结果
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ExcuteResultAsync(MQTTContext context)
        {
            return Task.Run(() =>
            {
                ExcuteResult(context);
            });
        }
    }
}
