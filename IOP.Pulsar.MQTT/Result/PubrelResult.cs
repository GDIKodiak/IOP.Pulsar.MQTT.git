﻿using System.Threading.Tasks;
using IOP.Models.Message.MQTT.Package;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// 发布释放结果
    /// </summary>
    public class PubrelResult : MQTTResult
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public PubrelResult()
        {
        }

        /// <summary>
        /// 执行结果
        /// </summary>
        /// <param name="context"></param>
        public override void ExcuteResult(MQTTContext context)
        {
            var pubrel = (PubrelPackage)context.Request.Package;
            PubcompPackage package = new PubcompPackage(pubrel.PacketIdentifier);
            context.Response.Package = package;
            context.Response.SendPackage(package);
        }
        /// <summary>
        /// 执行结果(异步)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ExcuteResultAsync(MQTTContext context)
        {
            return Task.Run(() =>
            {
                ExcuteResult(context);
            });
        }
    }
}
