﻿using IOP.Models.Message.MQTT;
using IOP.Models.Message.MQTT.Package;
using System.Threading.Tasks;

namespace IOP.Pulsar.MQTT.Result
{
    /// <summary>
    /// 发布结果
    /// </summary>
    public class PublishResult : MQTTResult
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public PublishResult()
        {
        }
        /// <summary>
        /// 执行结果
        /// </summary>
        /// <param name="context"></param>
        public override void ExcuteResult(MQTTContext context)
        {
            var publish = (PublishPackage)context.Request.Package;
            switch (publish.QoS)
            {
                case QoSType.QoS1:
                    PubackPackage puback = new PubackPackage(publish.PacketIdentifier);
                    context.Response.Package = puback;
                    context.Response.SendPackage(puback);
                    return;
                case QoSType.QoS2:
                    PubrecPackage pubrec = new PubrecPackage(publish.PacketIdentifier);
                    context.Response.Package = pubrec;
                    context.Response.SendPackage(pubrec);
                    return;
            }
        }
        /// <summary>
        /// 执行结果(异步)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Task ExcuteResultAsync(MQTTContext context)
        {
            return Task.Run(() =>
            {
                ExcuteResult(context);
            });
        }
    }
}
