﻿namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// MQTT数据上下文
    /// </summary>
    public class MQTTContext
    {
        /// <summary>
        /// 请求
        /// </summary>
        public MQTTRequest Request { get; set; }

        /// <summary>
        /// 响应
        /// </summary>
        public MQTTResponse Response { get; set; }

        /// <summary>
        /// 会话
        /// </summary>
        public readonly Session Session;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="session"></param>
        public MQTTContext(Session session)
        {
            Session = session;
        }
    }
}
