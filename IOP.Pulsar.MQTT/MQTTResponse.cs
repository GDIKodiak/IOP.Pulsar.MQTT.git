﻿using IOP.Models.Message.MQTT.Package;
using IOP.Models.Net;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// MQTT响应包
    /// </summary>
    public class MQTTResponse
    {
        /// <summary>
        /// 数据包
        /// </summary>
        public IMQTTPackage Package { get; set; }

        /// <summary>
        /// 连接的Socket
        /// </summary>
        private readonly SocketMonitor ConnectedSocket;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="socket"></param>
        public MQTTResponse(SocketMonitor socket)
        {
            ConnectedSocket = socket;
        }

        /// <summary>
        /// 发送数据包
        /// </summary>
        /// <param name="package"></param>
        public void SendPackage(IMQTTPackage package)
        {
            ConnectedSocket.ConnectedSocket.Send(package.ToBytes());
        }
        /// <summary>
        /// 发送并关闭连接
        /// </summary>
        /// <param name="package"></param>
        public void SendAndClose(IMQTTPackage package)
        {
            ConnectedSocket.ConnectedSocket.Send(package.ToBytes());
            ConnectedSocket.ConnectedSocket.Disconnect(true);
        }
        /// <summary>
        /// 关闭连接
        /// </summary>
        public void Close()
        {
            if (ConnectedSocket.ConnectedSocket.Connected)
                ConnectedSocket.ConnectedSocket.Disconnect(true);
        }
    }
}
