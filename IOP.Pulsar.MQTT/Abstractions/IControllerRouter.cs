﻿using System;
using System.Collections.Concurrent;
using System.Reflection;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 主题路由接口
    /// </summary>
    public interface IControllerRouter
    {
        /// <summary>
        /// 控制器集合
        /// </summary>
        ConcurrentDictionary<Type, ConcurrentQueue<MQTTController>> Controllers { get; set; }
        /// <summary>
        /// 获取控制器方法
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        MethodInfo GetControllerMethod(string filter);
    }
}
