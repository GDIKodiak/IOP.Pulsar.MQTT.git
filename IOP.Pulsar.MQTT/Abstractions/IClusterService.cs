﻿using IOP.Models.Message.MQTT.Package;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 集群服务
    /// </summary>
    public interface IClusterService
    {
        /// <summary>
        /// 集群发布
        /// </summary>
        /// <param name="package"></param>
        void ClusterPublish(PublishPackage package);
    }
}
