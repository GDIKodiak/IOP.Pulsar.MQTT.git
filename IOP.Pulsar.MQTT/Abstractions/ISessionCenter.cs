﻿using IOP.Models.Message.MQTT.Package;
using IOP.Models.Net;

namespace IOP.Pulsar.MQTT.Abstractions
{
    /// <summary>
    /// 会话中心
    /// </summary>
    public interface ISessionCenter
    {
        /// <summary>
        /// 创建会话
        /// </summary>
        /// <param name="socket">目标潜逃字</param>
        /// <param name="package">连接报文</param>
        /// <returns></returns>
        Session CreateSession(SocketMonitor socket, ConnectPackage package);

        /// <summary>
        /// 销毁会话
        /// </summary>
        /// <param name="session">目标会话</param>
        void DisposeSession(Session session);
    }
}
