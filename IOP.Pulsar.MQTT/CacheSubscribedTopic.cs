﻿using IOP.Models.Message.MQTT;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// 缓存用订阅的主题
    /// </summary>
    public class CacheSubscribedTopic
    {
        /// <summary>
        /// 订阅的QoS等级
        /// </summary>
        public QoSType SubscribedQoS { get; set; }
        /// <summary>
        /// 主题
        /// </summary>
        public string Topic { get; set; }
    }
}
