﻿namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// 会话状态
    /// </summary>
    public enum CacheSessionState
    {
        /// <summary>
        /// 未登录
        /// </summary>
        NotLogin,
        /// <summary>
        /// 已登录
        /// </summary>
        IsLogin
    }
}
