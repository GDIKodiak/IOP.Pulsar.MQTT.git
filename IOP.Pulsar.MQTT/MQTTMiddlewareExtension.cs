﻿using IOP.Pulsar.Abstractions;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// 中间件扩展
    /// </summary>
    public static class MQTTMiddlewareExtension
    {
        /// <summary>
        /// 使用MQTT控制中心
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IProductLineBuilder<MQTTContext> UseMQTTControlCenter(this IProductLineBuilder<MQTTContext> builder)
        {
            return builder.UseMiddleware<MQTTControlCenterMiddleware>();
        }
    }
}
