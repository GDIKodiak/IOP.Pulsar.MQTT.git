﻿using IOP.Models.Message.MQTT.Package;
using IOP.Models.Net;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// MQTT请求包
    /// </summary>
    public class MQTTRequest
    {
        /// <summary>
        /// 包
        /// </summary>
        public readonly IMQTTPackage Package;
        /// <summary>
        /// 连接的Socket
        /// </summary>
        private readonly SocketMonitor ConnectedSocket;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="package">包</param>
        /// <param name="socket">当前嵌套字</param>
        public MQTTRequest(IMQTTPackage package, SocketMonitor socket)
        {
            Package = package;
            ConnectedSocket = socket;
        }
    }
}
