﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// 缓存用会话
    /// </summary>
    public class CacheSession
    {
        /// <summary>
        /// 回话状态
        /// </summary>
        public CacheSessionState State { get; set; }
        /// <summary>
        /// 客户端Id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// 订阅的主题
        /// </summary>
        public List<CacheSubscribedTopic> SubscribedTopics { get; set; }
    }
}
