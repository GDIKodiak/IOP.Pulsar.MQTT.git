﻿using IOP.Models.Message.MQTT;
using IOP.Models.Message.MQTT.Package;
using System;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// MQTT连接异常
    /// </summary>
    public class MQTTConnectException : Exception
    {
        /// <summary>
        /// 连接报文
        /// </summary>
        public readonly ConnectPackage Package;

        /// <summary>
        /// 返回码
        /// </summary>
        public ReturnCodeType ReturnCode { get; private set; } = ReturnCodeType.ClientIdError;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="package"></param>
        /// <param name="message"></param>
        public MQTTConnectException(ConnectPackage package, string message)
            : base(message)
        {
            Package = package;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="package"></param>
        /// <param name="returnCode"></param>
        /// <param name="message"></param>
        public MQTTConnectException(ConnectPackage package, ReturnCodeType returnCode, string message)
            : base(message)
        {
            Package = package;
            ReturnCode = returnCode;
        }
    }
}
