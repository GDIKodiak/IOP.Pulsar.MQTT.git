﻿using IOP.Models.Message.MQTT;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// 订阅的主题
    /// </summary>
    public class SubscribedTopic
    {
        /// <summary>
        /// 订阅的QoS等级
        /// </summary>
        public QoSType SubscribedQoS { get; set; }
        /// <summary>
        /// 订阅的主题
        /// </summary>
        public Topic Topic { get; set; }
    }
}
