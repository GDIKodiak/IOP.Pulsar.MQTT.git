﻿namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// MQTT常量
    /// </summary>
    public class MQTTConstant
    {
        /// <summary>
        /// 连接
        /// </summary>
        public const string Connect = "CONNECT";
        /// <summary>
        /// 发布
        /// </summary>
        public const string Publish = "PUBLISH";
        /// <summary>
        /// 发布回执
        /// </summary>
        public const string Puback = "PUBACK";
        /// <summary>
        /// 发布确认
        /// </summary>
        public const string Pubrec = "PUBREC";
        /// <summary>
        /// 发布释放
        /// </summary>
        public const string Pubrel = "PUBREL";
        /// <summary>
        /// 发布完成
        /// </summary>
        public const string Pubcomp = "PUBCOMP";
        /// <summary>
        /// 订阅
        /// </summary>
        public const string Subscribe = "SUBSCRIBE";
        /// <summary>
        /// 取消订阅
        /// </summary>
        public const string Unsubscribe = "UNSUBSCRIBE";
        /// <summary>
        /// 心跳请求
        /// </summary>
        public const string Pingreq = "PINGREQ";
        /// <summary>
        /// 断开连接
        /// </summary>
        public const string Disconnet = "DISCONNECT";
        /// <summary>
        /// 集群发布
        /// </summary>
        public const string ClusterPublish = "/ClusterPublish";
        /// <summary>
        /// 缓存主题
        /// </summary>
        public const string CacheTopices = "CacheTopices";
        /// <summary>
        /// 主题前缀
        /// </summary>
        public const string TopicPrefix = "Topic_";
        /// <summary>
        /// 会话前缀
        /// </summary>
        public const string SessionPrefix = "Session:";
    }
}
