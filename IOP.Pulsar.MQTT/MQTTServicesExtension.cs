﻿using IOP.Pulsar.Abstractions;
using IOP.Pulsar.MQTT.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace IOP.Pulsar.MQTT
{
    /// <summary>
    /// MQTT服务扩展
    /// </summary>
    public static class MQTTServicesExtension
    {
        /// <summary>
        /// 添加MQTT服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddMQTT(this IServiceCollection services)
        {
            services.AddSingleton<ITopicCenter, TopicCenter>();
            services.AddSingleton<IControllerRouter, ControllerRouter>();
            services.AddSingleton<ISessionCenter, SessionCenter>();
            services.AddProductLineEntry<MQTTProductLineEntry, MQTTContext>();
            return services;
        }
    }
}
